import React,{Component} from 'react';
import NavigationDropdown from '../src/Components/NavigationDropdown';
import Hamburger from '../src/Components/Hamburger';
import Carousel from '../src/Components/Carousel';
import HobbiesHolder from '../src/Components/Hobbies';
import Searchbar from '../src/Components/Inputfield/SearchBar';
import SearchButton from '../src/Components/Buttons/SearchButton';
import SkillHolder from '../src/Components/SkillHolder';
import InfoSection from '../src/Components/InfoSection';
import reactSkillImage from '../src/Components/images/reactjs.svg';
import nodeSkillImage from '../src/Components/images/node2.svg';
import jsSkillImage from '../src/Components/images/js.svg';
import pythonSkillImage from '../src/Components/images/pythonww.svg';
import PicSlideHolder from '../src/Components/PicSlideHolder';
import Slide1 from '../src/Components/images/slide1.svg';
import OtherSkillHolder from '../src/Components/OtherSkillHolder';
import photoshop from '../src/Components/images/photoshop.svg';
import illustrator from '../src/Components/images/illustrator.svg';
import corel from '../src/Components/images/corel1.svg';
import php from '../src/Components/images/php.svg';
import wordpress from '../src/Components/images/wordpress.svg';
import xd from '../src/Components/images/xd.svg';


class  App extends Component {

  

constructor(props) {
  super(props);
  this.state = {
    navActive: false,
  }
  this.navToggle = this.navToggle.bind(this);
}


navToggle() {
  this.setState({
    ...this.state,
    navActive: !this.state.navActive
  })
}
  render(){
    

    return (
      <div className="App">
        <div className="App-header">
            <h3 className="logo">MSA</h3>
            <div className="TopSearch">
              <Searchbar/>
              <SearchButton/>
            </div>
            <ul className="links">
              <li><a href="">Home</a></li>
              <li><a href="">Our Works</a></li>
              <li><a href="">Locate us</a></li>
              <li><a href="">Contact us</a></li>
            </ul>
            <Hamburger toggle={this.navToggle} active={this.state.navActive}/>
            <NavigationDropdown active={this.state.navActive}/>
            
        </div>
        <Carousel active={this.state.navActive}/>
        <div className="SkillCategories"><h2>MAJOR SKILL SET</h2></div>
        <div className="SkillHolder">
        <div>
        <SkillHolder><img src={reactSkillImage} alt="React Logo"/></SkillHolder>
        <SkillHolder><img src={nodeSkillImage} alt="React Logo"/></SkillHolder>
        </div>
        <div>
        <SkillHolder><img src={pythonSkillImage} alt="React Logo"/></SkillHolder>
        <SkillHolder><img src={jsSkillImage} alt="React Logo"/></SkillHolder>
        
        </div>
        </div>

        <PicSlideHolder left="" right=""><img src={Slide1} alt="slide"/></PicSlideHolder>
        <div><h2>OTHER SKILLS</h2></div>
        <OtherSkillHolder>
        <img src={photoshop} alt="React Logo"/>
        <img src={illustrator} alt="Photoshop Logo"/>
        <img src={corel} alt="React Logo"/>
        </OtherSkillHolder>
<p></p>
        <OtherSkillHolder>
        <img src={php} alt="React Logo"/>
        <img src={wordpress} alt="Photoshop Logo"/>
        <img src={xd} alt="React Logo"/>
        </OtherSkillHolder>

        <div class="hobbies">HOBBIES <div></div></div>
        <HobbiesHolder/>

        <InfoSection/>
        <div class="hobbies">DIGITAL PARTNERS</div>
        
        <footer><p>Copyright 2019</p></footer>
      </div>
    );
  }
  
}

export default App;
