import React from 'react'

const style={
    width:'135px',
    height:'26px',
    backgroundColor:'white',
    paddingLeft: '5px',
    boxShadow:'inset 3px 3px 10px 1px grey',
    border:'0px'
    
}

const Searchbar =()=>{
    return(
        <input type="text" className="Searchbar" style={style} placeholder="Search...">
        
        </input>
    )
}

export default Searchbar