import React from 'react';
import image1 from '../../src/Components/images/image1.gif';
import linkdin from '../../src/Components/images/linkdin.svg';
import './Carousel.css';


const Carousel =(props)=>{
    return(
        <div className="Carousel">
            <img src={image1} alt="working man"/>
            <div className="verticalLline">
            </div>
            <div className={`jobs${props.active ? ' off' : ''}`}>
            
                <h2>
                <p>Software Developer.</p>
                <p>Graphics Designer.</p>
                <p>Computer Engineer.</p>
                </h2>
            </div>

            <div className="pole">

            </div>
           

        </div>
     
    )
}

export default Carousel