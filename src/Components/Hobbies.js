import React from 'react';
import './Hobbies.css';


const Hobbies =(props)=>{
    return(
        <div className="HobbiesHolder">
            <div className="setA">
                <div className="col1">Coding</div>
                <div className="col2">Designing</div>
                <div className="col3">Swimming</div>
            </div>
            <div className="setB">
                <div className="col4">Researching</div>
                <div className="col5">Dancing</div>
                <div className="col6">Traveling</div>
                
            </div>       
                    
        </div>
    )
}

export default Hobbies