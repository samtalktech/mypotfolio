import React from 'react';
import './PicSlideHolder.css';


const PicSlideHolder =(props)=>{
    return(
        <div className="PicSlideHolder">
            <p className="rightScroll">{props.right}</p>
            <p className="leftScroll">{props.left}</p>
            <p className="imagesSlide">{props.children}</p>
        </div>
        
    )
}

export default PicSlideHolder