import React from 'react';
import './Hamburger.css'



const Hamburger =(props)=>{
    return(
        <div className="ham" onClick={props.toggle}>
        <div className= {`Hamburger${props.active ? ' rotateLeft' : ''}`} >
        </div>
        <div className={`Hamburger${props.active ? ' off' : ''}`}>
        </div>
        <div className={`Hamburger${props.active ? ' rotateRight' : ''}`}>
        </div>
        </div>
    )
}

export default Hamburger